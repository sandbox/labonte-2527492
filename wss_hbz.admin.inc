<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\hbz\Service;

/**
 * Implements a settings form callback.
 */
function wss_hbz_admin_settings() {
  $settings = array();

  $settings[Service::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate HBZ search'),
    '#default_value' => Service::getSetting(Service::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[Service::SETTING__ADDRESS] = array(
    '#type' => 'textfield',
    '#title' => t('Server address'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(Service::SETTING__ADDRESS),
    '#weight' => 20,
  );

  $settings[Service::SETTING__PORT] = array(
    '#type' => 'textfield',
    '#title' => t('Server port'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(Service::SETTING__PORT),
    '#weight' => 22,
  );

  $settings[Service::SETTING__DATABASE] = array(
    '#type' => 'textfield',
    '#title' => t('Database'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(Service::SETTING__DATABASE),
    '#weight' => 22,
  );

  $settings[Service::SETTING__SYNTAX] = array(
    '#type' => 'textfield',
    '#title' => t('Syntax'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(Service::SETTING__SYNTAX),
    '#weight' => 22,
  );

  $settings['#submit'] = array('wss_hbz_admin_settings_submit');

  return system_settings_form($settings);
}

/**
 * Implements a custom submit handler.
 */
function wss_hbz_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  }
}
