<?php
// $id:$

/**
 * @file
 * This file contains Edutags' Request class.
 */

namespace publicplan\wss\hbz;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  /**
   * Parameter: Search term.
   */
  const PARAM__SEARCH = 'search';

  /**
   * Parameter: Target host address.
   */
  const PARAM__ADDRESS = 'address';

  /**
   * Parameter: Target port.
   */
  const PARAM__PORT = 'port';

  /**
   * Parameter: Target database.
   */
  const PARAM__DATABASE = 'database';

  /**
   * Parameter: Record syntax
   */
  const PARAM__SYNTAX = 'syntax';

  /**
   * Parameter: Record offset.
   */
  const PARAM__OFFSET = 'offset';

  /**
   * Parameter: Record count.
   */
  const PARAM__COUNT = 'count';

  /**
   * Record syntax: MAB2 (default).
   */
  const SYNTAX_DEFAULT = '1.2.840.10003.5.16';

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Service specific caching table.
   */
  const CACHE = 'cache_wss_hbz';

  /**
   * HTTP request method.
   *
   * @var string
   */
  protected $method = self::METHOD__GET;

  /**
   * Resource identifier for yaz connection.
   *
   * @var resource
   */
  private $connection;

  /**
   * Set request parameters.
   *
   * @param array $parameters
   *   Needs at least the key 'search' containing a search term.
   *
   * @throws Exception
   */
  public function setParameters($parameters) {
    if (!isset($parameters[self::PARAM__SEARCH])) {
      throw \Exception("Missing 'search' parameter.");
    }

    if (!isset($parameters[self::PARAM__ADDRESS])) {
      $parameters[self::PARAM__ADDRESS] = Service::getSetting(Service::SETTING__ADDRESS);
    }
    if (!isset($parameters[self::PARAM__PORT])) {
      $parameters[self::PARAM__PORT] = Service::getSetting(Service::SETTING__PORT);
    }
    if (!isset($parameters[self::PARAM__DATABASE])) {
      $parameters[self::PARAM__DATABASE] = Service::getSetting(Service::SETTING__DATABASE);
    }
    if (!isset($parameters[self::PARAM__SYNTAX])) {
      $parameters[self::PARAM__SYNTAX] = Service::getSetting(Service::SETTING__SYNTAX);
    }
    if (!isset($parameters[self::PARAM__OFFSET])) {
      $parameters[self::PARAM__OFFSET] = 0;
    }
    if (!isset($parameters[self::PARAM__COUNT])) {
      $parameters[self::PARAM__COUNT] = 10;
    }
    parent::setParameters($parameters);

    return $this;
  }

  /**
   * Stage the request.
   *
   * @param bool $use_cache
   *   Whether to use caching.
   *
   * @return \publicplan\wss\hbz\Request
   *   Returns itself.
   */
  public function send($use_cache = TRUE) {
    Service::checkAvailabilityOfYazExtension();
    $connection_str = sprintf('%s:%s/%s',
      $this->parameters[self::PARAM__ADDRESS],
      $this->parameters[self::PARAM__PORT],
      $this->parameters[self::PARAM__DATABASE]
    );
    $this->connection = yaz_connect($connection_str);
    yaz_syntax($this->connection, $this->parameters[self::PARAM__SYNTAX]);
    $search_string = $this->parameters[self::PARAM__SEARCH];
//    $search_string = '@attr 1=4 "' . $this->parameters[self::PARAM__SEARCH] . '"';
//    $search_string = '@attr 1=7 "' . $this->parameters[self::PARAM__SEARCH] . '"';
    yaz_search($this->connection, 'rpn', $search_string);
    yaz_wait();
    $hits = yaz_hits($this->connection);
    $start = $this->parameters[self::PARAM__OFFSET] + 1;
    $count = $this->parameters[self::PARAM__COUNT];
    if ($start > $hits) {
      $start = 1;
    }
    if ($start + $count > $hits) {
      $count = $hits - $start + 1;
    }
    yaz_range($this->connection, $start, $count);
    yaz_present($this->connection);
    $response = '';
    for ($i = $start; $i <= $count; $i++) {
      $response .= yaz_record($this->connection, $i, 'raw') . Service::SEPERATOR__END_SET;
    }
    $this->response = Response::create()->setTotalHits($hits)->parse($this, $response);
    yaz_close($this->connection);

    return $this;
  }
}
