<?php
// $id:$

/**
 * @file
 * This file contains the Edutags specific implementation of the \SearchResponse
 * interface.
 */

namespace publicplan\wss\hbz;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Extract results.
   *
   * This method is senseless and just implemented for compatibility reasons.
   *
   * @return array
   *   Array of results/records.
   */
  public function extractResults() {
    $sets = explode(Service::SEPERATOR__END_SET, $this->body);
    $results = array_filter($sets, 'strlen');
    $this->data['hits'] = count($results);

    return $results;
  }

  /**
   * Do nothing.
   *
   * @return \publicplan\wss\hbz\Response
   *   Returns itself.
   */
  public function parseResponseData() {
    $this->body = Service::init()->decode($this->nativeResponse);
    $offset = $this->request->getParameter(Request::PARAM__OFFSET);
    $this->data['hits_from'] = $offset + 1;
    $this->data['hits_to'] = $offset + $this->request->getParameter(Request::PARAM__COUNT);

    return $this;
  }

  /**
   * Set total hit count.
   *
   * @param int $hits
   *   Number of matched records.
   *
   * @return \publicplan\wss\hbz\Response
   *   Returns itself.
   */
  public function setTotalHits($hits) {
    $this->data['total_hits'] = $hits;

    return $this;
  }
}
