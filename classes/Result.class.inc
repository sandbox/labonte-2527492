<?php
// $id:$

/**
 * @file
 * This file contains DigiBib specific implementation of Result class.
 */

namespace publicplan\wss\hbz;

use publicplan\wss\base\SearchResult;

class Result extends SearchResult {
  const MAB_FIELD__ID = 'field_id';
  const MAB_FIELD__NAME = 'field_name';
  const MAB_FIELD__FLAGS = 'field_flags';
  const MAB_FIELD__IS_ARRAY = 'field_is_array';
  const MAB_FIELD__CALLBACK = 'field_callback';

  /**
   * HAUPTSACHTITEL IN VORLAGEFORM ODER MISCHFORM
   *
   * Indikator:
   * blank = keine Nebeneintragung
   * a     = zusaetzliche Nebeneintragung unter dem Sachtitel
   * b     = zusaetzliche Nebeneintragung mit dem Sachtitel
   */
  const MAB__TITLE = 331;

  /**
   * VERFASSERANGABE
   *
   * Indikator:
   * blank = nicht definiert
   */
  const MAB__AUTHOR = 359;

  /**
   * INTERNATIONALE STANDARDBUCHNUMMER (ISBN)
   *
   * Indikator:
   * blank = ISBN formal nicht geprueft
   * a     = ISBN formal richtig
   * b     = ISBN formal falsch
   * z     = keine ISBN, aber Einbandart und/oder Preis
   */
  const MAB__ISBN = 540;

  /**
   * INTERNATIONALE STANDARDNUMMER FUER MUSIKALIEN (ISMN)
   *
   * Indikator:
   * blank = ISMN formal nicht geprueft
   * a     = ISMN formal richtig
   * b     = ISMN formal falsch
   * z     = keine ISMN, aber Einbandart und/oder Preis
   */
  const MAB__ISMN = 541;

  /**
   * INTERNATIONALE STANDARDNUMMER FUER FORTLAUFENDE
   * SAMMELWERKE (ISSN)
   *
   * Indikator:
   * blank = ISSN formal nicht geprueft
   * a     = ISSN formal richtig
   * b     = ISSN formal falsch
   * z     = keine ISSN, aber Einbandart und/oder Preis
   */
  const MAB__ISSN = 542;

  /**
   * INTERNATIONALE STANDARDNUMMER FUER REPORTS (ISRN)
   *
   * Indikator:
   * blank = ISRN formal nicht geprueft
   * a     = ISRN formal richtig
   * b     = ISRN formal falsch
   * z     = keine ISRN, aber Einbandart und/oder Preis
   */
  const MAB__ISRN = 543;

  /**
   * ELEKTRONISCHE ADRESSE UND ZUGRIFFSART
   * FUER EINE COMPUTERDATEI IM FERNZUGRIFF
   *
   * Indikator:
   *
   * Zugriffsmethode:
   * blank = nicht spezifiziert
   * a     = E-Mail
   * b     = FTP (File Transfer)
   * c     = Remote Login (Telnet)
   * d     = Dial-up (konventioneller Telefonanschluss)
   * e     = HTTP
   * h     = In Unterfeld $2 spezifizierte Zugriffsmethode
   *
   * Unterfelder:
   * $a    = Name des Host
   * $b    = IP-Zugriffsnummer
   * $c    = Art der Komprimierung
   * $d    = Zugriffspfad fuer eine Datei
   * $f    = Elektronischer Name der Datei im Verzeichnis des Host
   * $g    = URN (Uniform Resource Name)
   * $h    = Durchfuehrende Stelle einer Anfrage
   * $i    = Anweisung fuer die Ausfuehrung einer Anfrage
   * $j    = Datenuebertragungsrate (Bits pro Sekunde)
   * $k    = Passwort
   * $l    = Logon/Login-Angabe
   * $m    = Kontaktperson
   * $n    = Ort des Host
   * $o    = Betriebssystem des Host
   * $p    = Port
   * $q    = Elektronischer Dateiformattyp
   * $r    = Einstellungen fuer die Dateiuebertragung
   * $s    = Groesse der Datei
   * $t    = Unterstuetzte Terminal-Emulationen
   * $u    = URL (Uniform Resource Locator)
   * $v    = Oeffnungszeiten des Host fuer die gewaehlte Zugangsart
   * $w    = Identifikationsnummer des verknuepften Datensatzes
   * $x    = Interne Bemerkungen
   * $z    = Allgemeine Bemerkungen
   * $2    = Zugriffsmethode
   * $3    = Bezugswerk
   * $A    = Beziehung
   */
  const MAB__HREF = 655;

  /**
   * STICHWOERTER
   *
   * Indikator:
   * blank = nicht aufgegliedert
   * a     = Sachbegriff
   * b     = geographischer Begriff
   * c     = Personenname
   * d     = Koerperschaftsname
   */
  const MAB__KEYWORDS = 720;

  /**
   * MAB/MAB2 fields as seperated array values.
   *
   * @var array
   */
  protected $mabFields = array();

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Field mappings.
   *
   * @var array
   */
  protected $fieldMappings = array(
    array(
      self::MAB_FIELD__ID => self::MAB__TITLE,
      self::MAB_FIELD__NAME => 'title',
      self::MAB_FIELD__CALLBACK => 'mapTitle',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__AUTHOR,
      self::MAB_FIELD__NAME => 'author',
      self::MAB_FIELD__CALLBACK => 'mapAuthor',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__ISBN,
      self::MAB_FIELD__NAME => 'isbn',
      self::MAB_FIELD__CALLBACK => 'mapIsbn',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__ISMN,
      self::MAB_FIELD__NAME => 'ismn',
      self::MAB_FIELD__CALLBACK => 'mapIsmn',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__ISSN,
      self::MAB_FIELD__NAME => 'issn',
      self::MAB_FIELD__CALLBACK => 'mapIssn',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__ISRN,
      self::MAB_FIELD__NAME => 'isrn',
      self::MAB_FIELD__CALLBACK => 'mapIsrn',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__HREF,
      self::MAB_FIELD__NAME => 'href',
      self::MAB_FIELD__CALLBACK => 'mapHyperlink',
    ),
    array(
      self::MAB_FIELD__ID => self::MAB__KEYWORDS,
      self::MAB_FIELD__NAME => 'keywords',
      self::MAB_FIELD__IS_ARRAY => TRUE,
    ),
  );

  /**
   * Parse record for further processing.
   *
   * @param string $raw_data
   *   Raw record data.
   *
   * @return \publicplan\wss\hbz\Resu
   * lt
   *   Returns itself.
   */
  public function parse($raw_data) {
    $this->raw = $raw_data;
    $this->mabFields = $this->mabFieldSplit($this->raw);
    foreach ($this->fieldMappings as $field_mapping) {
      // Skip if the field does not exist:
      if (!isset($this->mabFields[$field_mapping[self::MAB_FIELD__ID]])) {
        continue;
      }
      // Handle fields with callbacks:
      if (isset($field_mapping[self::MAB_FIELD__CALLBACK])) {
        if (is_callable($field_mapping[self::MAB_FIELD__CALLBACK])) {
          call_user_func_array($field_mapping[self::MAB_FIELD__CALLBACK], array(
            $this->mabFields[$field_mapping[self::MAB_FIELD__ID]],
            $field_mapping,
          ));
        }
        elseif (is_callable(array($this, $field_mapping[self::MAB_FIELD__CALLBACK]))) {
          call_user_func_array(array($this, $field_mapping[self::MAB_FIELD__CALLBACK]), array(
            $this->mabFields[$field_mapping[self::MAB_FIELD__ID]],
            $field_mapping,
          ));
        }
      }
      // Handle generic fields:
      else {
        $field_values = array();
        foreach ($this->mabFields[$field_mapping[self::MAB_FIELD__ID]] as $field) {
          $field_values[] = $field['value'];
        }
        $this->data[$field_mapping[self::MAB_FIELD__NAME]] = implode("\n", $field_values);
      }
    }

    return $this;
  }

  /**
   * Split MAB/MAB2 fields.
   *
   * @return array
   *   Structured array to ease filling the actual data array.
   */
  protected function mabFieldSplit() {
    $fields = explode(Service::SEPERATOR__END_FIELD, $this->raw);
    $result = array();
    foreach ($fields as $field) {
      $this->mabField($field, $result);
    }

    return $result;
  }

  /**
   * Structurize field data.
   *
   * @param string $field
   *   The whole MAB field.
   * @param array $result_array
   *   Result array for field data.
   */
  protected function mabField($field, &$result_array) {
    $match = array();
    if (preg_match('/^(\d{3})([ a-z])/im', $field, $match)) {
      $field_value = substr($field, 4);
      $key = $match[1];
      if (!isset($result_array[$key])) {
        $result_array[$key] = array();
      }
      $this->mabSubfield($field_value, $match[2], $result_array[$key]);
    }

  }

  /**
   * Structurize the actual field value, optionally including sub-fields.
   *
   * @param string $data
   *   Field value.
   * @param string $field_flag
   *   Field flag.
   * @param array $subresult
   *   Result array for field value(s).
   */
  protected function mabSubfield($data, $field_flag, &$subresult) {
    $subfields = explode(Service::SEPERATOR__END_SUB_FIELD, $data);
    foreach ($subfields as $subfield) {
      if (empty($subfield)) {
        continue;
      }
      $subresult[] = array(
        'flag' => $field_flag,
        'value' => $subfield,
      );
    }
  }

  /**
   * Field callback for title.
   *
   * @param array $title_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapTitle($title_field, $mapping) {
    foreach ($title_field as $title) {
      if ($title['flag'] === ' ') {
        $this->data[$mapping[self::MAB_FIELD__NAME]] = isset($this->data[$mapping[self::MAB_FIELD__NAME]]) ?
          $title['value'] . ' ' . $this->data[$mapping[self::MAB_FIELD__NAME]] : $title['value'];
      }
      else {
        $this->data[$mapping[self::MAB_FIELD__NAME]] = isset($this->data[$mapping[self::MAB_FIELD__NAME]]) ?
          $this->data[$mapping[self::MAB_FIELD__NAME]] . ' ' . $title['value'] : $title['value'];
      }
    }
  }

  /**
   * Field callback for author.
   *
   * @param array $author_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping defintion.
   */
  protected function mapAuthor($author_field, $mapping) {
    $authors = array_map(function($a) {
      return $a['value'];
    }, $author_field);
    $this->data[$mapping[self::MAB_FIELD__NAME]] = implode('; ', $authors);
  }

  /**
   * Field callback for ISBN.
   *
   * @param array $isbn_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapIsbn($isbn_field, $mapping) {
    $isbn = array();
    foreach ($isbn_field as $field) {
//      $match = array();
//      $isbn_pattern = '/(^|.*[^\d\-])([\d\-]{10,17})([^\d\-].*|$)/';
//      if (preg_match($isbn_pattern, $field['value'], $match)) {
//        $isbn[] = $match[2];
//        break;
//      }
      $isbn[] = $field['value'];
    }
    $this->data[$mapping[self::MAB_FIELD__NAME]] = implode("\n", $isbn);
  }

  /**
   * Field callback for ISMN.
   *
   * @param array $ismn_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapIsmn($ismn_field, $mapping) {
    $ismn = array();
    foreach ($ismn_field as $field) {
//      $match = array();
//      $ismn_pattern = '/(^|.*[^\d\-])([\d\-]{10,17})([^\d\-].*|$)/';
//      if (preg_match($ismn_pattern, $field['value'], $match)) {
//        $ismn[] = $match[2];
//        break;
//      }
      $ismn[] = $field['value'];
    }
    $this->data[$mapping[self::MAB_FIELD__NAME]] = implode("\n", $ismn);
  }

  /**
   * Field callback for ISSN.
   *
   * @param array $issn_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapIssn($issn_field, $mapping) {
    $issn = array();
    foreach ($issn_field as $field) {
//      $match = array();
//      $issn_pattern = '/(^|.*[^\d\-])([\d\-]{10,17})([^\d\-].*|$)/';
//      if (preg_match($issn_pattern, $field['value'], $match)) {
//        $issn[] = $match[2];
//        break;
//      }
      $issn[] = $field['value'];
    }
    $this->data[$mapping[self::MAB_FIELD__NAME]] = implode("\n", $issn);
  }

  /**
   * Field callback for ISRN.
   *
   * @param array $isrn_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapIsrn($isrn_field, $mapping) {
    $isrn = array();
    foreach ($isrn_field as $field) {
//      $match = array();
//      $isrn_pattern = '/(^|.*[^\d\-])([\d\-]{10,17})([^\d\-].*|$)/';
//      if (preg_match($isrn_pattern, $field['value'], $match)) {
//        $isbn[] = $match[2];
//        break;
//      }
      $isrn[] = $field['value'];
    }
    $this->data[$mapping[self::MAB_FIELD__NAME]] = implode("\n", $isrn);
  }

  /**
   * Field callback for Hyperlink.
   *
   * @param array $href_field
   *   Field values array.
   * @param array $mapping
   *   Field mapping definition.
   */
  protected function mapHyperlink($href_field, $mapping) {
    foreach ($href_field as $href) {
      if ($href['flag'] === 'e') {
        $subflag = substr($href['value'], 0, 1);
        $value = substr($href['value'], 1);
        if ($subflag === 'u') {
          $this->data[$mapping[self::MAB_FIELD__NAME]] = $value;
        }
      }
    }
  }

  /**
   * Not yet implemented.
   *
   * @return string
   *   Hyperlink to the appropriate record or empty string if not set.
   */
  public function getResourceLocation() {
    return !empty($this->data['href']) ? $this->data['href'] : '';
  }

  /**
   * Return the unique record identifier.
   *
   * @return string
   *   Identifier string.
   */
  public function getServiceId() {
    return '';
  }
}
