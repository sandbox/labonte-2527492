<?php
// $id:$

/**
 * @file
 * Edutags Service class implementation.
 */

namespace publicplan\wss\hbz;

use publicplan\wss\base\SearchService;

class Service extends SearchService {
  /**
   * Service name/identifier.
   */
  const NAME = 'hbz';

  /**
   * Configiuration variable names.
   */
  const SETTING__ACTIVATED = 'wss_hbz_settings__activated';
  const SETTING__ADDRESS = 'wss_hbz_settings__address';
  const SETTING__PORT = 'wss_hbz_settings__port';
  const SETTING__DATABASE = 'wss_hbz_settings__database';
  const SETTING__SYNTAX = 'wss_hbz_settings__syntax';

  /**
   * Special char: "Satzende".
   */
  const SEPERATOR__END_SET = "\x1d";

  /**
   * Special char: "Feldende".
   */
  const SEPERATOR__END_FIELD = "\x1e";

  /**
   * Special char: "Unterfeld".
   */
  const SEPERATOR__END_SUB_FIELD = "\x1f";

  /**
   * Character mapping from MAB2 encoding to UTF-8.
   *
   * @var array
   */
  protected $characterMapping = array(
//    self::SEPERATOR__END_SET => "\n\n\n",
//    self::SEPERATOR__END_FIELD => "\n\n",
//    self::SEPERATOR__SUB_FIELD => "\n",
    // Nicht-Sortier-Zeichen Anfang:
    "\x88" => '',
    // Nicht-Sortier-Zeichen Ende:
    "\x89" => '',
    // Doppeltes Anfuehrungszeichen unten:
    "\xa2" => "\xe2\x80\x9e",
    // Dollar:
    "\xa4" => "\x24",
    // Kreuz:
    "\xa6" => "\xe2\x80\xa0",
    // Minute:
    "\xa8" => "\xe2\x80\xb2",
    // Beginn eines Zitats: einfaches Anfuehrungszeichen links oben:
    "\xa9" => "\xe2\x80\x98",
    // Beginn eines Zitats: doppeltes Anfuehrungszeichen links oben:
    "\xaa" => "\xe2\x80\x9c",
    // Erniedrigungszeichen (Notenschrift):
    "\xac" => "\xe2\x99\xad",
    // Copyright:
    "\xad" => "\xc2\xa9",
    // Published-Vermerk:
    "\xae" => "\xe2\x84\x97",
    // Eingetragenes Warenzeichen:
    "\xaf" => "\xc2\xae",
    // Ain, Ajin:
    "\xb0" => "\xca\xbb",
    // Hamza, Alef:
    "\xb1" => "\xca\xbc",
    // Einfaches Anfuehrungszeichen links unten:
    "\xb2" => "\xe2\x80\x9a",
    // Teilfeldtrennzeichen (Doppelkreuz):
    "\xb6" => "\xe2\x80\xa1",
    // Sekunde:
    "\xb8" => "\xe2\x80\xb3",
    // Einfaches Anfuehrungszeichen rechts oben:
    "\xb9" => "\xe2\x80\x99",
    // Doppeltes Anfuehrungszeichen rechts oben:
    "\xba" => "\xe2\x80\x9d",
    // Erhoehungszeichen (Notenschrift):
    "\xbc" => "\xe2\x99\xaf",
    // Weiches Zeichen:
    "\xbd" => "\xca\xb9",
    // Hartes Zeichen:
    "\xbe" => "\xca\xba",
    // Hochtonzeichen:
    "\xc0" => "\xcc\x89",
    // Gravis:
    "\xc1" => "\xcc\x80",
    // Akut:
    "\xc2" => "\xcc\x81",
    // Zirkumflex:
    "\xc3" => "\xcc\x82",
    // Tilde:
    "\xc4" => "\xcc\x83",
    // Balken:
    "\xc5" => "\xcc\x84",
    // Halbkreis:
    "\xc6" => "\xcc\x86",
    // Punkt:
    "\xc7" => "\xcc\x87",
    // Trema:
    "\xc8" => "\xcc\x88",
    // Umlaut Ä:
    "\xc9\x41" => "\xc3\x84",
    // Umlaut ä:
    "\xc9\x61" => "\xc3\xa4",
    // Umlaut Ö:
    "\xc9\x4f" => "\xc3\x96",
    // Umlaut ö:
    "\xc9\x6f" => "\xc3\xb6",
    // Umlaut Ü:
    "\xc9\x55" => "\xc3\x9c",
    // Umlaut ü:
    "\xc9\x75" => "\xc3\xbc",
    // Ringel:
    "\xca" => "\xcc\x8a",
    // Apostroph nachgesetzt:
    "\xcb" => "\xcc\x95",
    // Cedille uebergesetzt:
    "\xcc" => "\xcc\x92",
    // Doppelakut:
    "\xcd" => "\xcc\x8b",
    // Haken angesetzt:
    "\xce" => "\xcc\x9b",
    // Hacek:
    "\xcf" => "\xcc\x8c",
    // Cedille:
    "\xd0" => "\xcc\xa7",
    // Rechtscedille:
    "\xd1" => "\xcc\x9c",
    // Sedila:
    "\xd2" => "\xcc\xa6",
    // Ogonek:
    "\xd3" => "\xcc\xa8",
    // Ringel untergesetzt:
    "\xd4" => "\xcc\xa5",
    // Halbkreis untergesetzt:
    "\xd5" => "\xcc\xae",
    // Punkt untergesetzt:
    "\xd6" => "\xcc\xa3",
    // Trema untergesetzt:
    "\xd7" => "\xcc\xa4",
    // Unterstreichungsstrich:
    "\xd8" => "\xcc\xb2",
    // Doppelte Unterstreichungsstriche:
    "\xd9" => "\xcc\xb3",
    // Vertikaler Unterstrich:
    "\xda" => "\xcc\xa9",
    // Zirkumflex untergesetzt:
    "\xdb" => "\xcc\xad",
    // Doppeltilde bzw. -bogen, 1. Teil:
    "\xdd" => "\xef\xb8\xa0",
    // Doppelbogen 2. Teil:
    "\xde" => "\xef\xb8\xa1",
    // Doppeltilde 2. Teil:
    "\xdf" => "\xef\xb8\xa3",
    // Ligatur aus A und E:
    "\xe1" => "\xc3\x86",
    // Serbokroatisches stimmhaftes dentales Dj:
    "\xe2" => "\xc4\x90",
    // Ligatur aus I und J:
    "\xe6" => "\xc4\xb2",
    // Polnisches hartes L:
    "\xe8" => "\xc5\x81",
    // Skandinavisches Oe:
    "\xe9" => "\xc3\x98",
    // Ligatur aus O und E:
    "\xea" => "\xc5\x92",
    // Islaendisches Thorn:
    "\xec" => "\xc3\x9e",
    // Ligatur aus a und e:
    "\xf1" => "\xc3\xa6",
    // Serbokroatisches stimmhaftes dentales dj:
    "\xf2" => "\xc4\x91",
    // Islaendisches Eth:
    "\xf3" => "\xc3\xb0",
    // Tuerkisches dumpfes i:
    "\xf5" => "\xc4\xb1",
    // Ligatur aus i und j:
    "\xf6" => "\xc4\xb3",
    // Polnisches hartes l:
    "\xf8" => "\xc5\x82",
    // Skandinavisches oe:
    "\xf9" => "\xc3\xb8",
    // Ligatur aus o und e:
    "\xfa" => "\xc5\x93",
    // Scharfes s:
    "\xfb" => "\xc3\x9f",
    // Islaendisches Thorn:
    "\xfc" => "\xc3\xbe",
  );

  /**
   * Check whether the yaz extension is loaded.
   *
   * @param bool $throw_exception
   *   Whether to throw an exception, if the extension is not loaded.
   *
   * @return bool
   *   TRUE if the extension is loaded.
   * @throws \Exception
   */
  public static function checkAvailabilityOfYazExtension($throw_exception = TRUE) {
    $is_available = extension_loaded('yaz');
    if (!$is_available) {
      $is_available = dl('yaz.so');
    }

    if (!$is_available && $throw_exception) {
      throw new \Exception("Missing YAZ extension. Please install/enable it or you won't be able to use the wss_hbz module.");
    }

    return $is_available;
  }

  /**
   * Decode MAB2 (ISO-5426) to UTF-8.
   *
   * @param string $input_str
   *   MAB2 input string.
   *
   * @return string
   *   UTF-8 output string.
   */
  public function decode($input_str) {
    $input_mapping = array_keys($this->characterMapping);
    $output_mapping = array_values($this->characterMapping);
    $output_str = str_replace($input_mapping, $output_mapping, $input_str);
    return $output_str;
  }

//  public static function buildArray() {
//    header('Content-Type: text/html; charset=utf-8');
//
//    $callback1 = function($matches) {
//      $lc_match = strtolower($matches[1]);
//      return "\\u$lc_match";
//    };
//
//    $callback2 = function($matches) {
////      var_dump($matches);
//      array_shift($matches);
//      $return = '';
//      foreach ($matches as $match) {
//        $lc_match = strtolower($match);
//        $return .= "\\x$lc_match";
//      }
//
//      return $return;
//    };
//
//    foreach (self::$characterMapping as $key => $char) {
//      $cnt = 0;
//      $json_char = '["' . preg_replace_callback('/u([0-9A-Fa-f]{4})/i', $callback1, $char) . '"]';
//      $utf8char = json_decode($json_char);
//      $result_key = '"' . preg_replace_callback('/([0-9A-Fa-f]{2})/i', $callback2, bin2hex($key)) . '"';
//      $result_value = '"' . preg_replace_callback('/([0-9A-Fa-f]{2})/i', $callback2, bin2hex($utf8char[0])) . '"';
//      $result_array[$result_key] = $result_value;
//    }
//
//
//    var_dump(self::$characterMapping);
//
//    print '<pre>';
//    print_r($result_array);
//    print '</pre>';
//  }
}
